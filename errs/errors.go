package errs

import (
	"fmt"
	"strings"
)

func Error(err error, msg ...string) {
	if err != nil {
		err = fmt.Errorf("error: %s%s", strings.Join(msg, ""), err.Error())
		panic(err)
	}
}
