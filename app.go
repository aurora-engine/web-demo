package main

import (
	"demo/server"
	"embed"
	"gitee.com/aurora-engine/aurora"
)

// 加载静态资源根目录下的所有内容
//
//go:embed static/*
var static embed.FS

// 加载配置文件
//
//go:embed application.yml
var cnf []byte

func main() {
	err := aurora.Run(&server.Server{
		Engine: aurora.New(
			// 启用 debug 日志
			aurora.Debug(),
			// 加载配置
			aurora.LoadConfig(cnf),
			// 加载静态资源
			aurora.Static(static),
		),
	})
	if err != nil {
		panic(err)
	}
}
