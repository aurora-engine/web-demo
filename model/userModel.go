package model

import "time"

// UserModel 用户模型
// column tag 定义SGO模型字段映射
type UserModel struct {
	UserId      string `column:"user_id"  json:"userId,omitempty"`
	UserAccount string `column:"user_account" json:"userAccount,omitempty"`
	// Server() 中注册了一个 check 自定义约束 请求入参会进行校验验证
	UserEmail    string `column:"user_email" json:"userEmail,omitempty" constraint:"check"`
	UserPassword string `column:"user_password" json:"userPassword,omitempty"`
	UserName     string `column:"user_name" json:"userName,omitempty"`
	UserAge      int    `column:"user_age" json:"userAge,omitempty"`

	// time.Time 类型 在SGO中属于内置类型 默认解析格式为 yyyy-mm-dd hh:mm:ss
	UserBirthday    time.Time `column:"user_birthday" json:"userBirthday"`
	UserHeadPicture string    `column:"user_head_picture" json:"userHeadPicture,omitempty"`
	UserCreateTime  string    `column:"user_create_time" json:"userCreateTime,omitempty"`
}
