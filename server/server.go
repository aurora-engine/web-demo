package server

import (
	"community/errs"
	"database/sql"
	"demo/controller/user"
	"demo/mapper"
	"errors"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitee.com/aurora-engine/aurora"
	"gitee.com/aurora-engine/gobatis"
	"github.com/tencentyun/cos-go-sdk-v5"
)

type Server struct {
	*aurora.Engine
	User *user.Control
}

// Server 初始化依赖
func (s *Server) Server() {

	// 注册 user 包的处理器
	s.Use(user.New())

	// 注册 参数校验器
	s.Constraint("check", func(value any) error {
		if strings.Index(value.(string), "@") == -1 {
			return errors.New("账号格式错误")
		}
		return nil
	})

	// 获取配置实例
	config := s.GetConfig()

	// Gorm MySql 配置
	ormurl := config.GetString("aurora.database.url")
	sqlDB, err := sql.Open("mysql", ormurl)
	errs.Error(err)
	err = sqlDB.Ping()
	errs.Error(err)
	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	sqlDB.SetMaxIdleConns(10)
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	sqlDB.SetMaxOpenConns(100)
	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	sqlDB.SetConnMaxLifetime(time.Hour)

	// 初始化 SGO
	build := gobatis.New(sqlDB)
	build.Source("static/db/mappers")
	demoMapper := new(mapper.DemoMapper)
	build.ScanMappers(demoMapper)
	// 加载 SGO 和 Mapper 实例 到容器
	s.Use(build, demoMapper, sqlDB)

	// 腾讯云对象存储配置
	ossConfig := config.GetStringMapString("aurora.oss")
	u, _ := url.Parse(ossConfig["bucketurl"])
	su, _ := url.Parse(ossConfig["serviceurl"])
	b := &cos.BaseURL{BucketURL: u, ServiceURL: su}
	client := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  ossConfig["secretid"],
			SecretKey: ossConfig["secretkey"],
		},
	})
	// 加载到容器
	s.Use(client)
}

// Router 注册路由
func (s *Server) Router() {
	u := s.User
	s.Get("/login", u.Login)

	s.Post("/register", u.RegisterUser)

	s.Get("/index", func() string {

		return "index.html"
	})
}
