package user

import (
	"context"
	"database/sql"
	"demo/errs"
	"demo/mapper"
	"demo/model"
	"demo/msg"
	"errors"
	"fmt"
	"gitee.com/aurora-engine/aurora/utils/timeutils"
	"gitee.com/aurora-engine/aurora/utils/uuidutils"
	"gitee.com/aurora-engine/aurora/web"
	"github.com/tencentyun/cos-go-sdk-v5"
	"time"
)

// Control user 处理器实例
// 内部 属性均由项目启动期间初始化完成
type Control struct {
	*sql.DB
	*mapper.DemoMapper
	Cos *cos.Client // 对象存储
	// value tag 读取配置文件属性
	BucketURL string `value:"aurora.oss.BucketURL"`
}

// New 返回一个Controller 构造器 用于初始化 Use加载
func New() web.Constructor {
	return func() any {
		return &Control{}
	}
}

func (control *Control) Login(args LoginArgs) {

}

// RegisterUser 注册用户
// *web.MultipartFile 解析上传文件
// info 解析附带的请求体 json 数据
func (control *Control) RegisterUser(img *web.MultipartFile, info *model.UserModel) msg.Result {
	uuid := uuidutils.NewUUID()
	info.UserId = uuid
	info.UserBirthday = time.Now()
	info.UserCreateTime = timeutils.DateTime()

	// 校验用户邮箱
	email, err := control.SelectUserByEmail(map[string]any{"user_email": info.UserEmail})
	if err != nil {
		return msg.Database(err)
	}
	if email != 0 {
		return msg.Error(errors.New("注册失败"), "邮箱号已被注册")
	}

	begin, err := control.Begin()
	// begin 使用提供的事务处理
	err = control.InsertOneUser(begin, info)
	if err != nil {
		return msg.Database(err)
	}
	// 创建成功 上传头像
	if img == nil {
		begin.Rollback()
		return msg.Error(errors.New("没有上传头像"))
	}
	headers, ok := img.File["img"]
	if !ok {
		begin.Rollback()
		return msg.Error(errors.New("没有上传头像"))
	}
	open, err := headers[0].Open()
	defer open.Close()
	errs.Error(err)
	// 存储图片 存储路径更具文件路径自动创建
	// 创建 用户头像存储路径
	path := fmt.Sprintf("/%s/heade/%s", uuid, headers[0].Filename)
	_, err = control.Cos.Object.Put(context.Background(), path, open, nil)
	if err != nil {
		begin.Rollback()
		return msg.Error(errors.New("头像上传失败，用户创建未成功"))
	}
	// 保存用户头像地址
	info.UserHeadPicture = control.BucketURL + path
	err = control.UpdateUserImg(begin, map[string]any{
		"id":  info.UserId,
		"img": info.UserHeadPicture,
	})
	if err != nil {
		begin.Rollback()
		return msg.Database(err)
	}

	// 提交自定义事务
	begin.Commit()

	return msg.Ok(nil, "注册成功")
}
