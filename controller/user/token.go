package user

import (
	"community/app/msg"
	"community/model"
	"errors"
	"fmt"
	"gitee.com/aurora-engine/aurora/utils/timeutils"
	"gitee.com/aurora-engine/aurora/web"
	"github.com/golang-jwt/jwt"
	"strings"
	"time"
)

type Token struct {
	jwt.StandardClaims
	ID string
}

// TokenInfo 解析token
func TokenInfo() web.Middleware {
	return func(ctx web.Context) bool {
		request := ctx.Request()
		get := request.Header.Get("Authorization")
		if get == "" {
			ctx.Return(msg.Error(errors.New("身份验证失败"), "请登录"))
			return false
		}
		if strings.HasPrefix(get, "Bearer ") {
			get = get[7:]
		}
		// 解析 token
		parse, err := jwt.Parse(get, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); ok {
				return []byte("all"), nil
			}
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		})
		if err != nil {
			ctx.Return(msg.Error(errors.New("身份验证失败"), err.Error()))
			return false
		}
		// 存入上下文
		claims := parse.Claims.(jwt.MapClaims)
		ctx["token"] = &Token{
			ID: claims["ID"].(string),
		}
		return true
	}
}

func CreateToken(user model.UserModel) string {
	s := timeutils.DateTime()
	parse, err := time.Parse("2006-01-02 15:04:05", time.Now().Add(time.Duration(10*24*60*60)*time.Second).Format("2006-01-02 03:04:05"))
	if err != nil {
		panic(err)
	}
	claims := jwt.NewWithClaims(jwt.SigningMethodHS512, Token{
		StandardClaims: jwt.StandardClaims{
			Issuer:    s,
			ExpiresAt: parse.Unix(),
		},
		ID: user.UserId,
	})
	key := []byte("all")
	signingString, err := claims.SignedString(key)
	if err != nil {
		panic(err)
	}
	return signingString
}

// GetToken Token 初始化函数
func GetToken(ctx web.Context) any {
	t := ctx["token"]
	if t == nil {
		t = &Token{}
	}
	return t
}
