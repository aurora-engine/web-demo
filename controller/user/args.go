package user

type LoginArgs struct {
	Email    string `constraint:"check"`
	Password string
}

// RegisterArgs 用户注册参数
type RegisterArgs struct {
	Email    string `constraint:"check"  model:"false" type:"string" json:"email"` //电子邮件 不能为空
	Password string `model:"false" type:"string" json:"password"`                  //密码不能为空
}
