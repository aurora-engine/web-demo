# web-demo
web-demo 是 aurora 的一个小开发示例，其中的初始化和接口定义包含了绝大多数的功能点使用方法，
通过这个demo的学习，可以快速学习并上手使用 aurora 开始简单的 web应用程序的构建。

## 项目结构
本Demo的项目结构并不代表所有aurora项目都如此，根据开发者喜好和规范即可。
### 接口
`controller` 文件夹下面定义了 `user` 包，包中定义了登录注册的案例
### 模型
`model` 中定义了一个对应数据库表的对应模型，数据库表的定义在 `web-demo/static/db/sql/demo.sql` 中可以看到

### 数据库
数据库操作使用的是 `SGO` 关系映射框架，其中相关的定义 mapper执行函数定义在 `mapper`包下，mapper文件定义在 
`web-demo/static/db/mappers/demo-mapper.xml` 下

### 配置文件
配置文件则是项目根路径上的 `application,yml`,其中定义了基础的配置信息，数据库连接信息等...
### 静态资源
`static` 文件夹作为静态资源存放，本案例中暂时没有添加前端页面的示例，后续会更新到demo中。

### 启动入口
启动 Web 程序，需要编写一个继承 `*aurora.Engine` 的服务器，并实现 `aurora.Application` 接口中的两个方法,`Server()` 和 `Router()`
，其中一部分已经由 `*aurora.Engine` 实现。 通过启动器 `aurora.Run` 即可启动程序。
