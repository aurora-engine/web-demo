package msg

const (
	MQ        = "RabbitMQ"
	DB        = "gorm"
	WebSocket = "WebSocket"
)
