package msg

const (
	ServiceError   = "9999" // 业务失败
	ServiceSuccess = "0000" // 业务成功
	DatabaseError  = "8888" // 数据库操作失败
)
