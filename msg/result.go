package msg

import "strings"

// Result 请求处理统一返回结果
type Result struct {
	Code string `json:"code"` // 状态码
	Msg  string `json:"msg"`  // 附加消息
	Data any    `json:"data"` //响应数据
	Err  error  `json:"err"`  //错误信息
}

// Ok 业务处理成功
func Ok(data any, msg ...string) Result {
	join := strings.Join(msg, "")
	return Result{
		Code: ServiceSuccess,
		Msg:  join,
		Data: data,
	}
}

// Error 业务处理失败
func Error(e error, msg ...string) Result {
	join := strings.Join(msg, "")
	return Result{
		Code: ServiceError,
		Msg:  join,
		Err:  e,
	}
}

// Database 数据库操作失败
func Database(err error, msg ...string) Result {
	return Result{
		Code: DatabaseError,
		Msg:  "数据库操作出错",
		Err:  err,
	}
}
