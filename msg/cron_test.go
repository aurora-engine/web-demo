package msg

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"testing"
	"time"
)

func TestCron(t *testing.T) {
	secondParser := cron.NewParser(cron.Second | cron.Minute |
		cron.Hour | cron.Dom | cron.Month | cron.DowOptional | cron.Descriptor)
	c := cron.New(cron.WithParser(secondParser), cron.WithChain())
	_, err := c.AddFunc("0 0 0 * * ?", func() { fmt.Println("Every hour on the half hour") })
	if err != nil {
		t.Error(err)
		return
	}
	c.Start()
	time.Sleep(100 * time.Second)
	c.Stop()
	fmt.Println("end")
}
