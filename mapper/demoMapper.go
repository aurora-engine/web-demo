package mapper

import "database/sql"

// DemoMapper 定义一个测试 Mapper 匹配 web-demo/static/db/mappers/demo-mapper.xml 的映射文件
type DemoMapper struct {
	SelectUserByEmail func(any) (int, error)
	// InsertOneUser UpdateUserImg 都采用了自定义手动提交事务
	InsertOneUser func(*sql.Tx, any) error
	UpdateUserImg func(*sql.Tx, any) error
}
